﻿using CardGame;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace CardGameTests
{
    [TestClass]
    public class Task2Tests
    {
        [TestMethod]
        public void Test1()
        {
            var player = new Player("Player1", new Deck(40));

            Assert.IsFalse(player.TryDrawCard(out var card));

            var addedCard = new NumericalCard(CardNumber.Five);
            player.AddCard(addedCard);

            Assert.IsTrue(player.TryDrawCard(out card));

            Assert.AreEqual(card, addedCard);
        }

    }
}
