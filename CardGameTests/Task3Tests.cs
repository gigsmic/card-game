﻿using CardGame;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace CardGameTests
{
    [TestClass]
    public class Task3Tests
    {
        [TestMethod]
        public void Test1()
        {
            var card1 = new NumericalCard(CardNumber.Three);
            var card2 = new NumericalCard(CardNumber.Five);
            var card3 = new NumericalCard(CardNumber.Five);

            Assert.AreEqual(card1.CompareTo(card2), -1);
            Assert.AreEqual(card2.CompareTo(card1), 1);
            Assert.AreEqual(card2.CompareTo(card3), 0);
        }

        [TestMethod]
        public void Test2()
        {

            var player1 = new Player("Player1", new Deck(40));
            var player2 = new Player("Player2", new Deck(40));

            var table = new Table(40);
            table.AddPlayer(player1, 40);
            table.AddPlayer(player2, 40);

            player1.AddCard(new NumericalCard(CardNumber.Two));
            player2.AddCard(new NumericalCard(CardNumber.Two));

            player1.AddCard(new NumericalCard(CardNumber.Eight));
            player2.AddCard(new NumericalCard(CardNumber.Five));

            Assert.AreEqual(player1.NumberOfCards(), 2);
            Assert.AreEqual(player1.NumberOfCards(), 2);

            player1.TryDrawCard(out var card1);
            table.DrawCard(player1, card1);
            player2.TryDrawCard(out var card2);
            table.DrawCard(player2, card2);
            var val = card1.CompareTo(card2);
            Assert.AreEqual(val, 0);

            player1.TryDrawCard(out card1);
            table.DrawCard(player1, card1);
            player2.TryDrawCard(out card2);
            table.DrawCard(player2, card2);
            val = card1.CompareTo(card2);
            Assert.AreEqual(val, 1);

            table.DiscardAllCardsFromTable(player1);
            Assert.AreEqual(player1.NumberOfCards(), 4);
            Assert.AreEqual(player2.NumberOfCards(), 0);

        }

    }
}
