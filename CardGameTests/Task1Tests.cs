using CardGame;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace CardGameTests
{
    [TestClass]
    public class Task1Tests
    {
        [TestMethod]
        public void Test1()
        {
            var cards = NumericalCard.GetNewCards();
            var newDeck = new Deck(cards.Length);
            newDeck.AddCards(cards);

            Assert.AreEqual(newDeck.Size(), 40);
        }

        [TestMethod]
        public void Test2()
        {
            var cards = NumericalCard.GetNewCards();
            var newDeck = new Deck(cards.Length);

            newDeck.AddCards(cards);
            var nonShuffled = newDeck.TakeAllCards().ToList();

            newDeck.AddCards(cards);
            newDeck.Shuffle();
            var shuffled = newDeck.TakeAllCards().ToList();

            CollectionAssert.AreNotEqual(nonShuffled, shuffled);

        }
    }
}
