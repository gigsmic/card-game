Project is written in .Net Core 3.1
It can be run and tested with Visual Studio 2019.

Run using cmd:

dotnet build CardGame.sln
dotnet CardGame\bin\Debug\netcoreapp3.1\CardGame.dll


Run tests using cmd:

dotnet test CardGame.sln