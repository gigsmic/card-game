﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CardGame
{
    public class Table
    {
        private readonly Dictionary<Player, Deck> _seats = new Dictionary<Player, Deck>();
        private readonly int _maxDeckSize;

        public Table(int maxDeckSize)
        {
            _maxDeckSize = maxDeckSize;
        }
               
        public void AddPlayer(Player player, int maxDeckSize)
        {
            ConfirmAbsence(player);

            _seats[player] = new Deck(_maxDeckSize);
        }

        public void RemovePlayer(Player player)
        {
            ConfirmAttendance(player);

            _seats.Remove(player);
        }
            
        public void DrawCard(Player player, Card card)
        {
            ConfirmAttendance(player);

            _seats[player].AddCard(card);
        }
                
        public bool DiscardAllCardsFromTable(Player player)
        {
            ConfirmAttendance(player);

            bool won = false;
            foreach(var deck in _seats.Values)
            {
                won |= player.DiscardCards(deck.TakeAllCards());
            }
            return won;
        }

        /* Cards are returned to player when he has no more cards to play, and it was draw in last round */
        //public void ReturnCards()
        //{
        //    foreach (var (player, deck) in _seats)
        //    {
        //        deck.Shuffle();
        //        player.DiscardCards(deck.TakeAllCards());
        //    }
        //}

        public void ConfirmAttendance(Player player)
        {
            if (!_seats.ContainsKey(player))
            {
                throw new Exception("Player is not at the table.");
            }
        }

        public void ConfirmAbsence(Player player)
        {
            if (_seats.ContainsKey(player))
            {
                throw new Exception("Player is not at the table.");
            }
        }

    }
}
