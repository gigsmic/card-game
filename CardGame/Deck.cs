﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CardGame
{
    public class Deck
    {
        private static readonly Random _rng = new Random();

        private readonly List<Card> _deck;
        public int MaxSize { get; }

        public Deck(int size)
        {
            /*Note: think about size max value, it should be checked also*/
            if(size < 1)
            {
                throw new ArgumentException($"Deck size must be positive integer, {size} is invalid value");
            }
            MaxSize = size;
            _deck = new List<Card>(MaxSize);
        }

        public int Size() => _deck.Count();


        public void AddCard(Card card)
        {
            if(_deck.Count() + 1 > MaxSize)
            {
                throw new Exception("Deck is full");
            }
            _deck.Add(card);
        }

        public void AddCards(IEnumerable<Card> cards)
        {
            if (_deck.Count() + cards.Count() > MaxSize)
            {
                throw new Exception("Cards can't fit in deck.");
            }

            _deck.AddRange(cards);
        }

        public bool TryTakeCard(out Card card)
        {
            if(_deck.Count() == 0)
            {
                card = null;
                return false;
            }
            card = _deck[0];
            _deck.RemoveAt(0);
            return true;
        }

        public IEnumerable<Card> TakeAllCards()
        {
            var cards = (from c in _deck select c).ToList();
            _deck.Clear();
            return cards;
        }
        
        public void Shuffle()
        {
            var n = _deck.Count();
            while (n > 1)
            {
                n--;
                int k = _rng.Next(n + 1);
                var value = _deck[k];
                _deck[k] = _deck[n];
                _deck[n] = value;
            }
        }
    }
}
