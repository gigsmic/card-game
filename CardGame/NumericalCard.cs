﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CardGame
{
    public abstract class Card : IComparable
    {
        public abstract int CompareTo(object obj);
        public abstract override string ToString();
    }

    public enum CardNumber
    {
        One = 1,
        Two = 2,
        Three = 3,
        Four = 4,
        Five = 5,
        Six = 6,
        Seven = 7,
        Eight = 8,
        Nine = 9,
        Ten = 10
    }

    public class NumericalCard : Card
    {
        public CardNumber CardNumber { get; }

        public NumericalCard(CardNumber cardNumber)
        {
            CardNumber = cardNumber;
        }

        public override int CompareTo(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentException("Object cannot be null");
            }

            if (obj is NumericalCard otherCard)
                return CardNumber.CompareTo(otherCard.CardNumber);
            else
                throw new ArgumentException("Object is not a NumericalCard");
        }       

        public override string ToString() => CardNumber.ToString("d");

        public static Card[] GetNewCards()
        {
            var newDeck = new Card[40];

            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 10; j++)
                {
                    newDeck[i * 10 + j] = new NumericalCard((CardNumber)(j + 1));
                }
            return newDeck;
        }

    }
}
