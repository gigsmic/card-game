﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace CardGame
{
    public class Player : IEquatable<Player>
    {
        public string ID { get; } = Guid.NewGuid().ToString();

        private readonly string _name; 
        private Deck DrawPile;
        private Deck DiscardPile;

        private readonly int _maxDeckSize;

        public Player(string name, Deck drawPile)
        {
            _name = name;
            _maxDeckSize = drawPile.MaxSize;
            DrawPile = drawPile;
            DiscardPile = new Deck(_maxDeckSize);
        }

        public bool TryDrawCard(out Card card )
        {
            if(DrawPile.TryTakeCard(out card))
            {
                return true;
            }

            DiscardPile.Shuffle();
            var tmp = DiscardPile;
            DiscardPile = DrawPile;
            DrawPile = tmp;

            return DrawPile.TryTakeCard(out card);
        }

        public void AddCard(Card card) => DrawPile.AddCard(card);

        public int NumberOfCards() => DiscardPile.Size() + DrawPile.Size();

        public bool DiscardCards(IEnumerable<Card> cards)
        {
            DiscardPile.AddCards(cards);
            return NumberOfCards() == _maxDeckSize;
        }

        public override string ToString() => _name;

        public bool Equals([AllowNull] Player other) => other != null && other.ID == this.ID;
    }
}
