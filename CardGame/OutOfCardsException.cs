﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CardGame
{
    public class OutOfCardsException : Exception
    {
        public Player Player { get; }
        public OutOfCardsException(Player player) : base($"{player} has no cards!")
        {
            Player = player;
        }
    }
}
