﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CardGame
{
    public class Game
    {
        private Table _table;
        private IEnumerable<Card> _cards;
        private readonly int _maxDeckSize;

        private Player _player1, _player2;

        public Game(IEnumerable<Card> cards)
        {
            _maxDeckSize = cards.Count();
            _table = new Table(_maxDeckSize);
            _cards = cards;
        }

        public Player Play()
        {
            _player1 = new Player("Player1", new Deck(_maxDeckSize));
            _player2 = new Player("Player2", new Deck(_maxDeckSize));
            _table.AddPlayer(_player1, _maxDeckSize);
            _table.AddPlayer(_player2, _maxDeckSize);

            DealCards();

            while (true)
            {        
                try
                {
                    var res = PlayRound();
                    if(res > 0)
                    {
                        Console.WriteLine($"{_player1} wins this round.");
                        if (_table.DiscardAllCardsFromTable(_player1))
                        {
                            return _player1;
                        }
                    }
                    else if(res < 0)
                    {
                        Console.WriteLine($"{_player2} wins this round.");
                        if (_table.DiscardAllCardsFromTable(_player2))
                        {
                            return _player2;
                        }                        
                    }
                    else
                    {
                        Console.WriteLine($"Draw, winner of the next round takes all cards from table! ");
                    }
                }
                catch(OutOfCardsException ex)
                {
                    Console.WriteLine(ex.Message);

                    var player1NumberOfCards = _player1.NumberOfCards();
                    if (player1NumberOfCards == 0 && _player2.NumberOfCards() == 0)
                    {
                        return null;
                    }

                    if (player1NumberOfCards == 0)
                    {
                        return _player2;
                    }
                    else
                    {
                        return _player1;
                    }
                }

                Console.WriteLine();
            }
        }


        private void DealCards()
        {
            var newDeck = new Deck(_cards.Count());
            newDeck.AddCards(_cards);
            newDeck.Shuffle();

            while (newDeck.TryTakeCard(out var card))
            {
                _player1.AddCard(card);
                if(newDeck.TryTakeCard(out card))
                {
                    _player2.AddCard(card);
                }
                else
                {
                    break;
                }
            }


        } 

        private int PlayRound()
        {
            if (!_player1.TryDrawCard(out var card1))
            {
                throw new OutOfCardsException(_player1);
            }

            var p1NumberOfCards = _player1.NumberOfCards() + 1;
            Console.WriteLine($"{_player1} ({p1NumberOfCards} card{(p1NumberOfCards > 1 ? "s" : string.Empty )}): {card1}");
            _table.DrawCard(_player1, card1);

            if (!_player2.TryDrawCard(out var card2))
            {
                throw new OutOfCardsException(_player2);
            }
            var p2NumberOfCards = _player2.NumberOfCards() + 1;
            Console.WriteLine($"{_player2} ({p2NumberOfCards} card{(p2NumberOfCards > 1 ? "s" : string.Empty)}): {card2}");
            _table.DrawCard(_player2, card2);

            return card1.CompareTo(card2);            
        }

    }
}
