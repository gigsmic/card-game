﻿using System;
using System.Collections.Generic;

namespace CardGame
{
    class Program
    {
        static void Main(string[] args)
        {
            var cards = NumericalCard.GetNewCards();
            var game = new Game(cards);
            var player = game.Play();

            Console.WriteLine("");

            if(player != null)
            {
                Console.WriteLine($"{player} wins the game!");
            }
            else
            {
                Console.WriteLine("Both players were left without cards, it is draw game!");
            }

            Console.WriteLine("");
            Console.WriteLine("Press any key to finish.");
            Console.ReadKey();
        }
    }
}
